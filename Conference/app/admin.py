from django.contrib import admin
from app.models import *

# Register your models here.
admin.site.register(Track)
admin.site.register(Speaker)
admin.site.register(Session)